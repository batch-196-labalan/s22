

let koponanNiEugene = ["Eugene"];

koponanNiEugene.push("Vincent");
console.log(koponanNiEugene);

console.log(koponanNiEugene.push("Dennis")); //result number 3
console.log(koponanNiEugene);


let removedItem = koponanNiEugene.pop();  //remove last item and store in new variable
console.log(koponanNiEugene);
console.log(removedItem);



// UNSHIFT METHOD - add item infront of our array

let fruits = ["Mango", "Kiwi", "Apple"];
fruits.unshift("Pineapple");
console.log(fruits);



// .shift = remove item at the beginning
let computerBrands = ["Apple", "Acer", "Asus", "Dell"];
computerBrands.shift();
console.log(computerBrands);


// .splice = remove elements fropm a specified index and add elements
// all items from starting index will be deleted

computerBrands.splice(1);
console.log(computerBrands);

fruits.splice(0,1);  //start index & the number of items to be deleted from the starting point
console.log(fruits);

koponanNiEugene.splice(1,0,"Dennis","Alfred");  //starting index, 0 = add items from starting point without the deleting items
console.log(koponanNiEugene);

fruits.splice(0,2, "Lime", "Cherry"); //replaced items from the starting point
console.log(fruits);



// splice when removing any elements will be able to return the remove elements

let item = fruits.splice(0);  //empty whole number (affects the original) (2 indixes = affects the new variable)
console.log(fruits);
console.log (item);

let spiritDetective = koponanNiEugene.splice(0,1); //starting and the number of item to splice
console.log(spiritDetective);
console.log(koponanNiEugene);





// SORT() - sort elements in alphanumeric order
let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];
members.sort();
console.log(members);


let numbers = [50,100,12,10,1];
numbers.sort();
console.log(numbers);


//REVERSE  - backwards
members.reverse();
console.log(members);




// NON MUTATOR METHODs - change the original array when they are used
// return the index number of the first matching element in the array
//if no match was found it will return -1
//  useful in finding the index of item when total length of array is unknow and is contstanly being added or manipulated


// 1. .indexOf

let carBrands = ["Vios","Fortuner", "Crosswind", "city", "Vios", "Starex"];

let firstIndexOfVios = carBrands.indexOf("Vios");
console.log(firstIndexOfVios);


let indexOfStarex = carBrands.indexOf("Starex");
console.log(indexOfStarex);

let indexOfBeetle = carBrands.indexOf("Beetle"); // result -1 (no match found)
console.log(indexOfBeetle); 



// 2. lastIndexOf() - returns index of last matching element in the array

let lastIndexOfVios = carBrands.lastIndexOf("Vios");
console.log(lastIndexOfVios);

let lastIndexOfMio = carBrands.lastIndexOf("Mio"); //result -1
console.log(lastIndexOfMio);





// SLICE = copy a slice/portion of an array and return a new array from it

let shoeBrand = ["Jordan", "Nike", "Adidas", "Converse", "Sketchers"];

let myOwnedShoes = shoeBrand.slice(1);
console.log(myOwnedShoes);



//sliced starting, ending index) = allows us to copy an array into a new array w/ items from starting to just before the ending index
// the last index will not be included but the index befvore it

let herOwnedShoes = shoeBrand.slice(2,4); //result 2-3
console.log(herOwnedShoes);

let heroes = ["Captain america", "Superman", "Spiderman", "Wonder Woman", "Hulk","Hawkeye","Dr. Stranger"];

let myFavoriteHeroes = heroes.slice(2,6); //result 2-5
console.log(myFavoriteHeroes);




// TO STRING METHOD - return our array as a string separated by commas

let superHeroes = heroes.toString();
console.log(superHeroes);



// JOIN - return our array as a string by specified separator

let superHeroes2 = heroes.join(); // result is almost the same w/ tostring (no spaces)
console.log(superHeroes2);

let superHeroes3 = heroes.join(" "); //or join(", ")
console.log(superHeroes3);

let superHeroes4 = heroes.join(1);
console.log(superHeroes4);




// ITERATOR METHODS - single loops
// iterate or loops the items in an array

// 1. FOREACH - similar to a for loop wherein it is able to iterate over the items in an array
// it is able to repeat an action for each item in the array


// forEach() takes an argument which is a function. this function has no name and cannot be invoked outside our forEach this is what we call an anonymous function
// the function inside forEach is able to receive the current item being looped.

/*

heroes.forEach(function(hero){  //result: heroes in a list
	console.log(hero);
})

*/


let counter = 0
heroes.forEach(function(hero){  //result: heroes in a list
	counter++;
	console.log(counter);
	console.log(hero);  // console.log(counter, hero);
});



let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];


chessBoard.forEach(function(row){
	console.log(row);
})


// chessBoard.forEach(function(row){
// 	// console.log(row);
// 	row.forEach(function(square){
// 		console.log(square);
// 	});
// })



let numArr = [5,12,30,46,40];

numArr.forEach(function(number){

	if(number%5 === 0){
	console.log(number + " is divisible by 5");
    } else {
	console.log(number + " is not divisible by 5");
    }
})



// MAP () -same with forEach but whatever is returne in the function will be added into a new array we can save.
// returns a new array which contains the value/data returned by the function that was for each item in the original array

// let members = ["Ben", "Alan", "Alvin", "Jino", "Tine"];

let instructors = members.map(function(member){

	return member + " is an instructor";

})

console.log(instructors);
console.log(members);



let numArr2 = [1,2,3,4,5];

let squareMap = numArr2.map(function(number){

	return number * number

})

console.log(squareMap);




// MAP  VS. FOREACH

// map is able to return a new array
// foreach simply iterates and does not return anything




// INCLUDE -returns a boolean which determines if the item is in the array or not.
// returns true ift it is
// else returns false

let isAMember = members.includes("Tine");
console.log(isAMember);


let members5 = ["Ban", "Alarn", "Alvirn"];
console.log(members5);

members5 = members5.includes("Ban");
console.log(members5);



// array method - built in w/js allows us to manipulate our array 
// syntax = arrayname.method();


// mutator methods updates our arrays

// nun mutataor do not update

//forEach takes an anonymous function as an argument and repeats this function for each items in the array


// SORT THROUGH NUMBERS
var numArray = [140000, 104, 99,178,2500];
numArray.sort(function(a, b) {
  return a - b;
});

console.log(numArray);